What's with the name?

Development - The old way of thinking about software development makes it difficult to get feedback between decisions due to separation of analysis, design, physical design, implementation, testing, review, integration and deployment. These activities are separated in time.

Driven - We don't want to drive the development with Speculation.

Test - Automated, reified, concrete tests. TDD is not a testing technique. It's an analysis technique, a design technique, really a technique for all the activities of development.
