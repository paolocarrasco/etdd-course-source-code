Starter Test

Which test should you start with? Start by testing a variant of an operation that doesn't do anything.

The first question you have to ask with a new operation is 'Where does it belong?'. Until you've answered this questio, you don't know what to type for the test. Solve one problem at a time.  How can we answer just this question and no other?

If you write a realistic test first, you will find yourself solving a bunch of problems at once:

Where does the operation belong?
What are the correct inputs?
What is the correct output given those inputs?

Beginning with a realistic test will leave you too long without feedback. Red-green-refactor loop should be in minutes.

You can shorten the loop by choosing inputs and outputs that are trivially easy to discover. If you are familiar with the problem and confident that you can get it working quickly, then you can write a realistic test.

Polygon problem. Starter Test provides an answer:

- The output should be the same as the input
- The input should be as small as possible

Pick a starter test that will teach you something but that you are certain you can get working quickly. If you are implementing something for the Nth time, pick a test that will require an operation or two. You will be justifiably confident you can get it working.

Test case contains all the externally visible objects and messages.