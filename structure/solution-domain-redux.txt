Story of Gauss

Carl Friedrich Gauss (1777-1855) is one of the greatest mathematicians of all time. He was born before the computers were invented. During his lifetime he made significant contributions to almost every area of mathematics, as well as physics, astronomy and statistics. Gauss showed amazing mathematical skill from an early age.

Let me tell you a story when Gauss was just 8 years old. One day Gauss' teacher asked his class to add together all the numbers from 1 to 100, assuming that this task would occupy them for quite a while. He was shocked when young Gauss, after a few seconds thought, wrote down the answer 5050.

Gauss added the numbers in pairs - the first and the last, the second and the second to last and so on, observing that 1+100=101, 2+99=101, 3+98=101, ...so the total would be 50 lots of 101, which is 5050.

Gauss could have used his method to add all the numbers from 1 to any number - by pairing off the first number with the last, the second number with the second to last, and so on, he only had to multiply this total by half the last number, just one swift calculation.

    result = 0
    unless list.empty?
      n = list[list.size-1]
      result = (n * (n+1))/2
    end
    result

​There is a systematic way to make sure you have sufficient coverage instead of depending on confidence.

